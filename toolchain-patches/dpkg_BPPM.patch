commit 2e5b5c96c882976d151636ce6a52385dd309b194
Author: Ximin Luo <infinity0@debian.org>
Date:   Tue Oct 17 13:08:49 2017 +0200

    Support BUILD_PATH_PREFIX_MAP https://reproducible-builds.org/specs/build-path-prefix-map/

diff --git a/debian/changelog b/debian/changelog
index 9d2b28bd2..62ee55b6f 100644
--- a/debian/changelog
+++ b/debian/changelog
@@ -1,3 +1,16 @@
+dpkg (1.19.0.1.0~reproducible1) unstable; urgency=medium
+
+  * dpkg-buildpackage: Append ${pkg}_${ver} to the BUILD_PATH_PREFIX_MAP envvar
+    as described by https://reproducible-builds.org/specs/build-path-prefix-map/
+  * dpkg-buildflags: Don't set -fdebug-prefix-map by default, and explain why
+    in the documentation.
+  * dpkg-genbuildinfo: Also whitelist the BUILD_PATH_PREFIX_MAP envvar when
+    build-paths are requested to be written in the buildinfo.
+  * pkg-info.mk: Set and export the BUILD_PATH_PREFIX_MAP envvar, as described
+    by the aforementioned specification.
+
+ -- Ximin Luo <infinity0@debian.org>  Fri, 31 Mar 2017 15:58:56 -0400
+
 dpkg (1.19.0.1) unstable; urgency=medium
 
   * Packaging:
diff --git a/man/dpkg-buildflags.man b/man/dpkg-buildflags.man
index 541132e0b..3998fa3e9 100644
--- a/man/dpkg-buildflags.man
+++ b/man/dpkg-buildflags.man
@@ -470,13 +470,17 @@ This will cause warnings when the \fB__TIME__\fP, \fB__DATE__\fP and
 .
 .TP
 .B fixdebugpath
-This setting (enabled by default) adds
+This setting (disabled by default) adds
 .BI \-fdebug\-prefix\-map= BUILDPATH =.
 to \fBCFLAGS\fP, \fBCXXFLAGS\fP, \fBOBJCFLAGS\fP, \fBOBJCXXFLAGS\fP,
 \fBGCJFLAGS\fP, \fBFFLAGS\fP and \fBFCFLAGS\fP where \fBBUILDPATH\fP is
 set to the top-level directory of the package being built.
 This has the effect of removing the build path from any generated debug
-symbols.
+symbols.  However, other build tools may save these flags into other
+parts of the build output, thereby causing the overall package to be
+unreproducible again - which is why this setting is disabled by default.
+A newer better alternative is \fBBUILD_PATH_PREFIX_MAP\fP, which is set
+automatically by \fBdpkg-buildpackage\fP(1).
 .
 .SH ENVIRONMENT
 There are 2 sets of environment variables doing the same operations, the
diff --git a/man/dpkg-buildpackage.man b/man/dpkg-buildpackage.man
index bf9c6ace1..51ea977f9 100644
--- a/man/dpkg-buildpackage.man
+++ b/man/dpkg-buildpackage.man
@@ -523,6 +523,15 @@ This variable is set to \fIgain-root-command\fP when the field
 This variable is set to the Unix timestamp since the epoch of the
 latest entry in \fIdebian/changelog\fP, if it is not already defined.
 .
+.TP
+.B BUILD_PATH_PREFIX_MAP
+This variable has the value
+\fIsource-name\fP\fB_\fP\fIsource-version\fP\fB=\fP\fIBUILDPATH\fP
+appended onto it, prepended by \fB:\fP if the old value was non-empty.
+\fIsource-name\fP and \fIsource-version\fP are further encoded by
+replacing \fB%\fP with \fB%#\fP, \fB=\fP with \fB%+\fP, and \fB:\fP with
+\fB%.\fP.
+.
 .SH FILES
 .TP
 .I %PKGCONFDIR%/buildpackage.conf
diff --git a/scripts/Dpkg/Path.pm b/scripts/Dpkg/Path.pm
index f352cac35..0443ca7fe 100644
--- a/scripts/Dpkg/Path.pm
+++ b/scripts/Dpkg/Path.pm
@@ -30,6 +30,7 @@ our @EXPORT_OK = qw(
     get_pkg_root_dir
     guess_pkg_root_dir
     relative_to_pkg_root
+    path_prefix_map_append
 );
 
 use Exporter qw(import);
@@ -277,6 +278,34 @@ sub find_build_file($) {
     return;
 }
 
+sub path_prefix_map_enquote {
+    my $part = shift;
+    $part =~ s/%/%#/g;
+    $part =~ s/=/%+/g;
+    $part =~ s/:/%./g;
+    return $part;
+}
+
+=item $var = path_prefix_map_append($var, $dst, $src)
+
+Appends a new mapping "$dst=$src" onto the encoded prefix map in $var,
+according to the C<BUILD_PATH_PREFIX_MAP>
+L<specification|https://reproducible-builds.org/specs/build-path-prefix-map/>.
+$dst and $src are encoded as described therein, namely with
+
+    "%" substrings replaced by "%#"
+    "=" substrings replaced by "%+"
+    ":" substrings replaced by "%."
+
+=cut
+sub path_prefix_map_append($$$) {
+    my $curmap = shift;
+    my $dst = shift;
+    my $src = shift;
+    return (length($curmap) ? $curmap . ":" : "") .
+        path_prefix_map_enquote($dst) . "=" . path_prefix_map_enquote($src);
+}
+
 =back
 
 =head1 CHANGES
diff --git a/scripts/Dpkg/Vendor/Debian.pm b/scripts/Dpkg/Vendor/Debian.pm
index e0fd01113..ab92021ce 100644
--- a/scripts/Dpkg/Vendor/Debian.pm
+++ b/scripts/Dpkg/Vendor/Debian.pm
@@ -112,7 +112,7 @@ sub _add_build_flags {
         },
         reproducible => {
             timeless => 1,
-            fixdebugpath => 1,
+            fixdebugpath => 0,
         },
         sanitize => {
             address => 0,
diff --git a/scripts/dpkg-buildpackage.pl b/scripts/dpkg-buildpackage.pl
index affcca8d0..3c4779da0 100755
--- a/scripts/dpkg-buildpackage.pl
+++ b/scripts/dpkg-buildpackage.pl
@@ -43,7 +43,7 @@ use Dpkg::Version;
 use Dpkg::Control;
 use Dpkg::Control::Info;
 use Dpkg::Changelog::Parse;
-use Dpkg::Path qw(find_command);
+use Dpkg::Path qw(find_command path_prefix_map_append);
 use Dpkg::IPC;
 
 textdomain('dpkg-dev');
@@ -461,6 +461,9 @@ if ($changedby) {
 
 # <https://reproducible-builds.org/specs/source-date-epoch/>
 $ENV{SOURCE_DATE_EPOCH} ||= $changelog->{timestamp} || time;
+# <https://reproducible-builds.org/specs/build-path-prefix-map/>
+$ENV{"BUILD_PATH_PREFIX_MAP"} = path_prefix_map_append(
+    $ENV{"BUILD_PATH_PREFIX_MAP"}, "${pkg}_${version}", $cwd);
 
 # Check whether we are doing some kind of rootless build, and sanity check
 # the fields values.
diff --git a/scripts/dpkg-genbuildinfo.pl b/scripts/dpkg-genbuildinfo.pl
index 45c45089d..b1f6be384 100755
--- a/scripts/dpkg-genbuildinfo.pl
+++ b/scripts/dpkg-genbuildinfo.pl
@@ -250,11 +250,14 @@ sub collect_installed_builddeps {
 sub cleansed_environment {
     # Consider only whitelisted variables which are not supposed to leak
     # local user information.
+    my @env_whitelist = get_build_env_whitelist();
+    # See <https://reproducible-builds.org/specs/build-path-prefix-map>.
+    push(@env_whitelist, "BUILD_PATH_PREFIX_MAP") if ($use_feature{path});
     my %env = map {
         $_ => $ENV{$_}
     } grep {
         exists $ENV{$_}
-    } get_build_env_whitelist();
+    } @env_whitelist;
 
     # Record flags from dpkg-buildflags.
     my $bf = Dpkg::BuildFlags->new();
diff --git a/scripts/mk/pkg-info.mk b/scripts/mk/pkg-info.mk
index 15322cedd..6f42d3292 100644
--- a/scripts/mk/pkg-info.mk
+++ b/scripts/mk/pkg-info.mk
@@ -9,6 +9,10 @@
 #
 # SOURCE_DATE_EPOCH: the source release date as seconds since the epoch, as
 #   specified by <https://reproducible-builds.org/specs/source-date-epoch/>
+#
+# BUILD_PATH_PREFIX_MAP: the package name and version mapped to the top-level
+#   absolute package directory, as specified by
+#   <https://reproducible-builds.org/specs/build-path-prefix-map/>
 
 dpkg_late_eval ?= $(or $(value DPKG_CACHE_$(1)),$(eval DPKG_CACHE_$(1) := $(shell $(2)))$(value DPKG_CACHE_$(1)))
 
@@ -20,5 +24,14 @@ DEB_VERSION_UPSTREAM = $(call dpkg_late_eval,DEB_VERSION_UPSTREAM,echo '$(DEB_VE
 DEB_DISTRIBUTION = $(call dpkg_late_eval,DEB_DISTRIBUTION,dpkg-parsechangelog -SDistribution)
 
 SOURCE_DATE_EPOCH ?= $(call dpkg_late_eval,SOURCE_DATE_EPOCH,dpkg-parsechangelog -STimestamp)
-
 export SOURCE_DATE_EPOCH
+
+path_prefix_map_enquote = $(subst :,%.,$(subst =,%+,$(subst %,%\#,$(1))))
+BUILD_PATH_PREFIX_MAP ?= $(call dpkg_late_eval,BUILD_PATH_PREFIX_MAP,echo\
+"$${BUILD_PATH_PREFIX_MAP:+$$BUILD_PATH_PREFIX_MAP:}"$(call\
+path_prefix_map_enquote,$(DEB_SOURCE)_$(DEB_VERSION))=$(call\
+path_prefix_map_enquote,$(CURDIR)))
+export BUILD_PATH_PREFIX_MAP
+
+# FIXME: "export" makes dpkg_late_eval completely pointless since it forces
+# evaluation. This is true for both SOURCE_DATE_EPOCH and BUILD_PATH_PREFIX_MAP
diff --git a/scripts/t/dpkg_buildpackage.t b/scripts/t/dpkg_buildpackage.t
index 2b4345539..bdea5d859 100644
--- a/scripts/t/dpkg_buildpackage.t
+++ b/scripts/t/dpkg_buildpackage.t
@@ -44,6 +44,7 @@ delete $ENV{MAKEFLAGS};
 
 # Delete variables that can affect the tests.
 delete $ENV{SOURCE_DATE_EPOCH};
+delete $ENV{BUILD_PATH_PREFIX_MAP};
 
 # Delete other variables that can affect the tests.
 delete $ENV{$_} foreach grep { m/^DEB_/ } keys %ENV;
